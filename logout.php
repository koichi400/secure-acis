<?php
	session_start();
	unset( $_SESSION['sess_userid']);
	unset( $_SESSION['sess_username']);
	unset( $_SESSION['sess_fullname']);
	unset( $_SESSION['sess_position']);
	session_destroy();
	header('Location: index.php');
?>