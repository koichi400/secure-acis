<?php
    function grade_to_number($grade) {
        switch ($grade) {
            case "A":
                return 4;
                break;
            case "B":
                return 3;
                break;
            case "C":
                return 2;
                break;
            case "D":
                return 1;
                break;
            case "F":
                return 0;
                break;
        }
    }
?>