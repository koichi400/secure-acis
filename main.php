﻿<?php
    include "chksession.php";
    if ($sess_position != "STUDENT") {
        echo "THIS PAGE FOR STUDENT ONLY";
        exit();
    }
    include "config.php";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="SecureAcis - Secure Academic Information System" />
    <meta name="author" content="Koichi Atthawichian" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>SecureAcis - Secure Academic Information System</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <strong>Now logged in</strong> - <?=$sess_fullname?> (<?=$sess_username?>)
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header left-div">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="assets/img/logo.png" />
                </a>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a class="menu-top-active" href="main.php">Main</a></li>
                            <li><a href="password.php">Change Password</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Welcome</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <!--    Striped Rows Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Info
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="http://apollo.kmutt.ac.th/kmuttstdpic/default.aspx?&stdcode=<?=$sess_username?>" width="150px" alt="<?=$sess_fullname?>">
                                </div>
                                <div class="col-md-8">
                                    <strong>Name:</strong> <?=$sess_fullname?><br>
                                    <strong>ID:</strong> <?=$sess_username?><br>
                                    <strong>STATUS:</strong> <?=$sess_position?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  End  Striped Rows Table  -->
                </div>
                <div class="col-md-6">
                <!--    Striped Rows Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Semester 1/2017 Grade Announcement
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Course Code</th>
                                            <th>Result</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
    $sql = "SELECT * FROM sacis_regis WHERE regis_user='$sess_username'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $is_grade = TRUE;
        $count = 0;
        $all_score = 0;
        include "function.php";
        while ($row = $result->fetch_assoc()) {
?>
                                        <tr>
                                            <td><?=++$count?></td>
                                            <td><?=$row['regis_code']?></td>
                                            <td><?=$row['regis_grade']?></td>
                                        </tr>
<?php
    $all_score += grade_to_number($row['regis_grade']);
        }
    } else {
?>
                                        <tr>
                                            <td colspan="3"><center>NO REGISTRATION DATA</center></td>
                                        </tr>
<?php
    }
?>
                                    </tbody>
                                </table>
<?php
    if ($is_grade) {
        echo "GPA for this semester: ".($all_score/$count);
    }
?>
                            </div>
                        </div>
                    </div>
                    <!--  End  Striped Rows Table  -->
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2017 Koichi Atthawichian | By : <a href="http://www.designbootstrap.com/" target="_blank">DesignBootstrap</a>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>