﻿<?php
    include "chksession.php";
    include "config.php";
    $regis_post_password = mysqli_escape_string($conn, $_POST['regis_password']);
    $regis_post_password2 = mysqli_escape_string($conn, $_POST['regis_password2']);
    $message = "";
    if (!(empty($regis_post_password) || empty($regis_post_password2))) {
        if ($regis_post_password == $regis_post_password2) {
            $sql = "UPDATE sacis_user SET user_password='$regis_post_password' WHERE user_id=$sess_username";
            if (mysqli_query($conn, $sql)) {
                $message = "Password changed successfully";
            } else {
                $message =  "Error changing password: " . mysqli_error($conn);
            }
        } else {
            $message =  "Passwords you entered are not matched";
        }
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="SecureAcis - Secure Academic Information System" />
    <meta name="author" content="Koichi Atthawichian" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>SecureAcis - Secure Academic Information System</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <strong>Now logged in</strong> - <?=$sess_fullname?> (<?=$sess_username?>)
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header left-div">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="assets/img/logo.png" />
                </a>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
<?php
    if ($sess_position == "STUDENT") {
?>
                            <li><a href="main.php">Main</a></li>
<?php
    } else if ($sess_position == "ADMIN") {
?>
                            <li><a href="main-admin.php">Main</a></li>
                            <li><a href="profile.php">Student Profile</a></li>
                            <li><a href="grade.php">Grade Management</a></li>
<?php
    }
?>
                            <li><a class="menu-top-active" href="password.php">Change Password</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Change Password</h4>
                </div>
            </div>
<?php
if (!empty($message))  { 
?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <?=$message?>
                    </div>
                </div>
            </div>
<?php
}
?>
            <div class="row">
                <div class="col-md-6">
                    <form action="" method="post">
                        <label>Password :  </label>
                        <input name="regis_password"  type="password" class="form-control" />
                        <label>Confirm Password :  </label>
                        <input name="regis_password2"  type="password" class="form-control" />
                        <hr />
                        <input class="btn btn-info" type="submit" value="Change Password"></input>
                        <hr />
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="alert alert-info">
                        <strong>Attention:</strong>
                        <br>Users often forget their password... so we decided to not encrypt it anymore.
                        <br>Such a clever way! You can ask me anytime when you forgot!
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2017 Koichi Atthawichian | By : <a href="http://www.designbootstrap.com/" target="_blank">DesignBootstrap</a>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>
