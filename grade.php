﻿<?php
    include "chksession.php";
    if ($sess_position != "ADMIN") {
        echo "THIS PAGE FOR ADMIN ONLY";
        exit();
    }
    include "config.php";
    $regis_code = mysqli_escape_string($conn, $_GET['code']);
    $regis_post_id = mysqli_escape_string($conn, $_POST['regis_id']);
    $regis_post_grade = mysqli_escape_string($conn, $_POST['regis_grade']);
    $message = "";
    if (!(empty($regis_post_id) || empty($regis_post_grade))) {
        $sql = "UPDATE sacis_regis SET regis_grade='$regis_post_grade' WHERE regis_id=$regis_post_id";
        if (mysqli_query($conn, $sql)) {
            $message = "Record updated successfully";
        } else {
            $message =  "Error updating record: " . mysqli_error($conn);
        }
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="SecureAcis - Secure Academic Information System" />
    <meta name="author" content="Koichi Atthawichian" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>SecureAcis - Secure Academic Information System</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
     <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <strong>Now logged in</strong> - <?=$sess_fullname?> (<?=$sess_username?>)
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header left-div">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <img src="assets/img/logo.png" />
                </a>
            </div>
        </div>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="main-admin.php">Main</a></li>
                            <li><a href="profile.php">Student Profile</a></li>
                            <li><a class="menu-top-active" href="grade.php">Grade Management</a></li>
                            <li><a href="password.php">Change Password</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">Grade Management</h4>
                </div>
            </div>
<?php
if (!empty($message))  { 
?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <?=$message?>
                    </div>
                </div>
            </div>
<?php
}
?>
            <div class="row">
<?php
$sql_subject = "SELECT regis_code, COUNT(regis_id) AS regis_student FROM sacis_regis";
if (!empty($regis_code)) {
   $sql_subject = $sql_subject." WHERE regis_code = '$regis_code'";
}
$sql_subject = $sql_subject." GROUP BY regis_code";
$result_subject = mysqli_query($conn, $sql_subject);
$num_rows = mysqli_num_rows($result_subject);

if ($num_rows > 0) {
    if ($num_rows == 1) {
        $bs_layout = 12;
    } else {
        $bs_layout = 6;
    }
    while ($row_subject = $result_subject->fetch_assoc()) {
        $regis_code = $row_subject['regis_code'];
        $regis_student = $row_subject['regis_student'];
?>
                <div class="col-md-<?=$bs_layout?>">
                    <!--    Striped Rows Table  -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="grade.php?code=<?=$regis_code?>"><?=$regis_code?> Student List (Total: <?=$regis_student?>)</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Student Code</th>
                                            <th>Fullname</th>
                                            <th>Result</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
$sql = "SELECT * FROM sacis_regis INNER JOIN sacis_user ON regis_user = user_id WHERE regis_code = '$regis_code' ORDER BY regis_user";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $count = 0;
    while ($row = $result->fetch_assoc()) {
        $regis_grade = $row['regis_grade'];
?>
                                        <tr>
                                            <td><?=++$count?></td>
                                            <td><?=$row['regis_user']?></td>
                                            <td><?=$row['user_fullname']?></td>
                                            <td>
                                                <form action="grade.php" method="post">
                                                    <input type="hidden" name="regis_id" value="<?=$row['regis_id']?>">
                                                    <select name="regis_grade" onchange="this.form.submit();">
                                                        <option value="A"<?php if ($regis_grade == "A") echo " selected"?>>A</option>
                                                        <option value="B"<?php if ($regis_grade == "B") echo " selected"?>>B</option>
                                                        <option value="C"<?php if ($regis_grade == "C") echo " selected"?>>C</option>
                                                        <option value="D"<?php if ($regis_grade == "D") echo " selected"?>>D</option>
                                                        <option value="F"<?php if ($regis_grade == "F") echo " selected"?>>F</option>
                                                    </select>
                                                </form>
                                            </td>
                                        </tr>
<?php
    }
} else {
?>
                                        <tr>
                                            <td colspan="3"><center>NO REGISTRATION DATA</center></td>
                                        </tr>
<?php
}
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--  End  Striped Rows Table  -->
                </div>
<?php
        }
    }
?>
            </div>
        </div>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2017 Koichi Atthawichian | By : <a href="http://www.designbootstrap.com/" target="_blank">DesignBootstrap</a>
                </div>

            </div>
        </div>
    </footer>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>